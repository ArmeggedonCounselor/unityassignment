﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {
	void OnTriggerEnter(Collider other){
		if (other.gameObject.CompareTag ("Player")) {
			other.SendMessage ("SetScore", 1, SendMessageOptions.DontRequireReceiver);
			this.gameObject.SetActive (false);
		}
	}
}
