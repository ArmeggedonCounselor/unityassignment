﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Goal : MonoBehaviour {
	public Text message;
	public float messageTiming;
	private float messageTimer;
	bool removeMessage;

	void Start(){
		messageTimer = 0.0f;
		message.text = "";
		removeMessage = false;
	}

	void Update(){
		if (removeMessage) {
			messageTimer += Time.deltaTime;
		}
		if (messageTimer > messageTiming) {
			removeMessage = false;
			message.text = "";
		}
	}

	void OnTriggerEnter(Collider other){
		GameManager gm = other.GetComponent<GameManager> ();
		if (gm != null) {
			if (gm.victory) {
				SceneManager.LoadScene ("Win");
			} else {
				message.text = "You need more collectibles!";
				removeMessage = true;
			}
		}
	}
}
