﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	[Header("Data")]
	[Tooltip("This variable is the number of collectibles in the scene.")][SerializeField]
	private int maxCollectibles;
	[HideInInspector]public bool victory = false;
	private int currentCollectibles;
	private bool scoreChanged = false;

	[Header("Components")]
	[Tooltip("This holds the Text variable that will display the score.")][SerializeField]
	private Text score;
	[Tooltip("This holds the Text variable that will display when the goal is open.")][SerializeField]
	private Text goal;

	// Use this for initialization
	void Start () {
		currentCollectibles = 0;
		score.text = "Collectibles: " + currentCollectibles + " / " + maxCollectibles;
		goal.text = "";
	}
	
	// Update is called once per frame
	void Update () {
		if (scoreChanged) {
			scoreChanged = false;
			score.text = "Collectibles: " + currentCollectibles + " / " + maxCollectibles;
		}
		if (currentCollectibles == maxCollectibles) {
			victory = true;
			goal.text = "Goal Open!";
		}
	}

	public void SetScore(int increase){
		currentCollectibles += increase;
		scoreChanged = true;
	}
}
